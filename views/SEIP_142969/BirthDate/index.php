<?php
require_once ("../../../vendor/autoload.php");
use App\BirthDate\BirthDate;
use App\Message\Message;
use App\Utility\Utility;

$objBirthDate=new BirthDate();
$allData=$objBirthDate->index("obj");



################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$allData =  $objBirthDate->search($_REQUEST);
$availableKeywords=$objBirthDate->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################


######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBirthDate->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################



################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objBirthDate->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################


?>




<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../../../Resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets/font-awesome/css/font-awesome.min.css">
    <script src="../../../Resource/assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../Resource/assets/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../Resource/assets/css/style.css">

        <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../Resource/bootstrap/css/jquery-ui.css">
    <script src="../../../Resource/bootstrap/js/jquery-1.12.4.js"></script>
    <script src="../../../Resource/bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->

</head>



<body>
    


     <div class="container">
            <div class="row">
                <div class="">
                    <div class="col-lg-12 col-xm-1">
                        <div class="">
                               <?php include"menu.php" ?>
                        </div>
                    </div>
                </div>
            </div>
    </div>


    <div class="container">
            <div class="row">
                <div class="logo">
                    <div class="col-lg-12 col-xm-1">
                        <div class="">
                                <b><h2>Active List of Birth Date</h2></b>

                        </div>
                    </div>
                </div>
            </div>
    </div>




<div class="container">
            <div class="row">
                <div class="top-content">
                    <div class="col-lg-3 col-xm-1">
                        <div class="leftSideButton">
                            <a href="create.php" class="btn btn-info" role="button">Add New</a>
                            <a href="trashed.php" class="btn btn-primary" role="button">Trash List</a>

                        </div>
                    </div>

                    <div class="col-lg-8 col-xm-1">
                        <div class="download">
                            <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
                            <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
                            <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>

                        </div>
                    </div>
                </div>
            </div>
</div>
    
 


    <div class="container">
            <div class="row">
                <div class="searchForm">
                    <div class="col-lg-12 col-xm-1">
                        <div class="search">
    <!-- required for search, block 4 of 5 start -->


                <form id="searchForm" class="search" action="index.php"  method="get">
                    <input type="text" value="" id="searchID" name="search"  placeholder="Search" width="60" >
                    <input type="checkbox"  name="name"   checked  >Name
                    <input type="checkbox"  name="date"  checked >Date
                    <input hidden type="submit" class="btn-primary" value="search">
                </form>

     <!-- required for search, block 4 of 5 end -->


                        </div>
                    </div>
                </div>
            </div>
    </div>    
   

    <div class="container">
            <div class="row">
                <div class="">
                    <div class="col-lg-3 col-xm-1">
                    </div>
                    <div class="col-lg-6 col-xm-1">
                       <?php
echo "<table border='2px'>";
echo "<th style='text-align: center'>Serial</th>
<th style='text-align: center'>ID</th>
<th style='text-align: center'>Name</th>
<th style='text-align: center'>Birthdate</th>
<th style='text-align: center'>Action</th>";


foreach($someData as $oneData){
    echo "<tr style='height: 40px'>";
    echo "<td>".$serial."</td>";
    echo "<td>".$oneData->id."</td>";
    echo "<td>".$oneData->name."</td>";
    echo "<td>".$oneData->date."</td>";
    echo "<td>
            <a href='View.php?id=$oneData->id'><button class='btn btn-success'>View</button></a>
            <a href='edit.php?id=$oneData->id'><button class='btn btn-info'>Edit</button></a>
            <a href='trash.php?id=$oneData->id'><button class='btn btn-default'>Trash</button></a>
            <a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a>


        </td>";

    echo "</tr>";
    $serial++;
}//End of foreach loop

echo "</table>";
?>


                        </div>
                        <div class="col-lg-3 col-xm-1">
                    </div>
                    </div>
                </div>
            </div>
    </div>



     <div class="container">
            <div class="row">
                <div class="">
                     <div class="col-lg-5 col-xm-1">
                     </div>
                    <div class="col-lg-4 col-xm-1">
                        <div style="float:center">
                               
<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="left" class="container">
    <ul class="pagination">

        <?php
            $pageMinusOne=$page-1; // $pageMinusOne = $page-1;
            $pagePlusOne=$page+1; // $pagePlusOne = $page+1;

            if($page>$pages) Utility::redirect("index.php?Page=$pages");
            if($page>1) echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . '</a></li>';
            for($i=1;$i<=$pages;$i++)
            {
                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

            }
            if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . '</a></li>';
            ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->


                    <div class="col-lg-3 col-xm-1">

                     </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>







</body>

</html>



<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->




