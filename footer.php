	<section id="footer">
	<div class="container-fluid footer">
		<div class="container wpf">
			<div class="row">
				<div class="copyright"></div>	
					<div class="col-md-8">
						<p class="copyright-text">&copy 2016 & All  Right Reserved by # & Developed by <a href="#">Nazmun</a> </p>
					</div>			
					<div class="col-md-4">
						<div class="social-button">
						<p>Stay Connected</p>
						<ul>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						</ul>
						</div>					
					</div>					
			</div>
		</div>	
	</div>		
	</section>	
		
        <script src="Resource/assets/js/jquery.min.js"></script>
		<script src="Resource/assets/bootstrap/js/bootstrap.min.js"></script>

	<!-- Start Scroll UP -->
	<script src="js/jquery.scrollUp.js"></script>
	  <script type="text/javascript">
   $(function () {
  $.scrollUp({
    scrollName: 'scrollUp', // Element ID
    topDistance: '300', // Distance from top before showing element (px)
    topSpeed: 300, // Speed back to top (ms)
    animation: 'fade', // Fade, slide, none
    animationInSpeed: 200, // Animation in speed (ms)
    animationOutSpeed: 200, // Animation out speed (ms)
    scrollText: 'Scroll to top', // Text for element
    activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
  });
});
        </script>
	
	
	<!-- End  Scroll UP -->


		
    </body>
</html>