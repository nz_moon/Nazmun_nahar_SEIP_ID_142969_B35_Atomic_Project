<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Atomic roject at BITM</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
 <!-- CSS -->
 
    <link rel="stylesheet" href="Resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="Resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="Resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="Resource/assets/css/open-sans.css">
	<link rel="stylesheet" href="Resource/assets/css/animate.css">
    <link rel="stylesheet" href="Resource/assets/css/style.css">


    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="Resource/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="Resource/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="Resource/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="Resource/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="Resource/assets/ico/apple-touch-icon-57-precomposed.png">        
    </head>

    <body style="overflow: hidden; z-index: 100;">

        <!-- Add your site or application content here -->

		<section id="header">
		<div class="container-fluid">
		<div class="container header">
			<div class="row">
				<div class="logo">
					<div class="col-lg-8 col-xm-1">
						<div class="textlogo">
							
							<h1>
							Atomic project</h1>
							
						</div>
					</div>
				</div>
			</div>

<?php include"menu.php" ?>