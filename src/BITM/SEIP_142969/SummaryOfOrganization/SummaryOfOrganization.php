<?php

namespace App\SummaryOfOrganization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class SummaryOfOrganization extends DB{
    public $id;
    public $organization_name;
    public $organization_summary;

    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }


    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('organization_name', $postVariableData)) {
            $this->organization_name = $postVariableData['organization_name'];
        }

        if (array_key_exists('organization_summary', $postVariableData)) {
            $this->organization_summary = $postVariableData['organization_summary'];
        }
    }

    public function store()
    {
        $arrData=array($this->organization_name,$this->organization_summary);
        $sql = "Insert INTO summary_of_organiztion(organization_name,organization_summary) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Insurted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Insurted Successfully :( ");

        Utility::redirect('create.php');


    }//end of store mathod

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query("SELECT * from summary_of_organiztion WHERE is_deleted='No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from summary_of_organiztion WHERE id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arroneData  = $STH->fetch();
        return $arroneData;


    }// end of view();


    public function update(){

        $arrData = array ($this->organization_name, $this->organization_summary);
        $sql = "UPDATE summary_of_organiztion SET organization_name = ?, organization_summary = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()


    public function delete(){

        $sql = "DELETE from summary_of_organiztion where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()


    public function trash(){

        $sql = "UPDATE summary_of_organiztion SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()

    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from summary_of_organiztion where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update summary_of_organiztion SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();

    
    public function indexPaginator($page=0,$itemsPerPage=3){
        
        $start = (($page-1) * $itemsPerPage);
        
        $sql = "SELECT * from  summary_of_organiztion  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";
        
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
        
    }// end of indexPaginator();



    public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from  summary_of_organiztion  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();



    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['name']) && isset($requestArray['summary']) )  $sql = "SELECT * FROM `summary_of_organiztion` WHERE `is_deleted` ='No' AND (`organization_name` LIKE '%".$requestArray['search']."%' OR `organization_summary` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['name']) && !isset($requestArray['summary']) ) $sql = "SELECT * FROM `summary_of_organiztion` WHERE `is_deleted` ='No' AND `organization_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['name']) && isset($requestArray['summary']) )  $sql = "SELECT * FROM `summary_of_organiztion` WHERE `is_deleted` ='No' AND `organization_summary` LIKE '%".$requestArray['search']."%'";
        
        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        
        return $allData;
        
    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `summary_of_organiztion` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->organization_name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->organization_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->organization_summary);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->organization_summary);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords












}