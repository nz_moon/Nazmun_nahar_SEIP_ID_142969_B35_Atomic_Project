<?php


namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB{

    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();


    }

    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('book_title', $postVariableData)) {
            $this->book_title = $postVariableData['book_title'];
        }

        if (array_key_exists('author_name', $postVariableData)) {
            $this->author_name = $postVariableData['author_name'];
        }
    }

    public function store()
    {
        $arrData=array($this->book_title,$this->author_name);
        $sql = "Insert INTO book_title(book_title,author_name) VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Insurted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Insurted Successfully :( ");

        Utility::redirect('create.php');


    }//end of store mathod


	

    public function index($fetchMode='ASSOC'){

        $sql = "SELECT * from book_title where is_deleted = 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){
        $sql = 'SELECT * from book_title where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arroneData  = $STH->fetch();
        return $arroneData;


    }// end of view();


    public function update(){
        $arrData=array($this->book_title,$this->author_name);
        $sql="UPDATE book_title SET book_title = ?, author_name = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Updated Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Updated Successfully :( ");

        Utility::redirect('index.php');



    }//end of update method



    public function delete(){
        $arrData=array($this->book_title,$this->author_name);
        $sql="DELETE from book_title  WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute( $arrData);

        if($result)
            Message::setMessage("Success! Data Has Been Deleted Successfully :) ");
        else
            Message::setMessage("Failed! Data Has Not Been Deleted Successfully :( ");

        Utility::redirect('index.php');



    }//end of delete method


        public function trash(){

        $sql = "Update book_title SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()


    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from book_title where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update book_title SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();



     public function indexPaginator($page=0,$itemsPerPage=3){
        
        $start = (($page-1) * $itemsPerPage);
        
        $sql = "SELECT * from book_title  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";
        
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
        
    }// end of indexPaginator();




     public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();

    

    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_deleted` ='No' AND (`book_title` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `book_title` WHERE `is_deleted` ='No' AND `book_title` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_deleted` ='No' AND `author_name` LIKE '%".$requestArray['search']."%'";
        
        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        
        return $allData;
        
    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `book_title` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->book_title);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->book_title);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords









}

